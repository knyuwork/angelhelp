# AngelHelp #


![push-demo](./screenshots/AngelHelp_v1.gif)

## Demo Step

#####Step 1. Requestor takes the photo of the emergency 
#####Step 2. Helper discovers help request
#####Step 3. Click on the Flag of Requestor 
#####Step 4. View the image of the help to identify what is going on
#####Step 5. Accept the Request 
#####Step 6(Optional). Cancel the request if you can't help
 

## About the project

AngelHelp is a project consist of two apps, Requestor(Left) and Helper(Right).

The purpose of this project is to let people ask for nearby help.

## Why do we need this?

Emergency calls in Hong Kong have a response time 9 - 15 mins but not many cases can wait for such long.


