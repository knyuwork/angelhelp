import React from 'react';
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';

export default class Send extends React.Component {
  // shouldComponentUpdate(nextProps, nextState) {
  //   if (this.props.text.trim().length === 0 && nextProps.text.trim().length > 0 || this.props.text.trim().length > 0 && nextProps.text.trim().length === 0) {
  //     return true;
  //   }
  //   return false;
  // }  
  
  onPress() {
    if (this.props.text.trim().length > 0) {
      this.props.onSend({text: this.props.text.trim()}, true);
    }
  }

  render() {

    //if (this.props.text.trim().length > 0) {
      return (
        <View style={styles.container}>
          <TouchableOpacity
            style={[styles.container, this.props.containerStyle]}
            onPress={() => this.onPress()}
            accessibilityTraits="button"
          >
            <Text style={[styles.text, this.props.textStyle]}>{this.props.label}</Text>
          </TouchableOpacity>
        </View>

      );
    //}
    //return <View/>;
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(193,239,251,1)',
    flex: 1,
    borderRadius: 10,
    marginTop: 7,
    marginBottom: 7,
    marginLeft: 10,
    marginRight: 10

  },
  text: {    
    color: 'rgba(70,131,149,1)',
    fontWeight: '600',
    fontSize: 12,
    backgroundColor: 'transparent',
  },
});

Send.defaultProps = {
  text: '',
  onSend: () => {},
  label: 'Send',
  containerStyle: {},
  textStyle: {},
};

Send.propTypes = {
  text: React.PropTypes.string,
  onSend: React.PropTypes.func,
  label: React.PropTypes.string,
  containerStyle: View.propTypes.style,
  textStyle: Text.propTypes.style,
};
