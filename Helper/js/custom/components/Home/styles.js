
const React = require('react-native');

const { StyleSheet, Dimensions, Platform } = React;

const deviceHeight = Dimensions.get('window').height;

module.exports = StyleSheet.create({
  imageContainer: {
    flex: 1,
    width: null,
    height: null,
  },
  logoContainer: {
    flex: 1,
    marginTop: deviceHeight / 16,
    marginBottom: 30,
    alignItems: 'center',
    justifyContent: 'flex-start',

  },
  logo: {
    position: 'absolute',
    left: (Platform.OS === 'android') ? 40 : 50,
    //top: (Platform.OS === 'android') ? 35 : 60,
    flexDirection: 'row',
    width: 280,
    height: 150,
  },
  titleFont: {
    marginTop: 20,
    fontSize: 50,
    lineHeight: 50,
    backgroundColor: 'rgba(0,0,0,0)'
  },
  subTitleFont: {
    marginTop: 10,
    fontSize: 25,
    lineHeight: 25,
    backgroundColor: 'rgba(0,0,0,0)',
    fontFamily: 'SFNS Display',
    fontWeight: "300",
  },

  text: {
    color: '#D8D8D8',
    bottom: 6,
  },
  buttonContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
});
