
import React, { Component } from 'react';
import { Image, TouchableOpacity, Text, TextInput, AsyncStorage } from 'react-native';
import { connect } from 'react-redux';
import { Container, Button, View, H3 } from 'native-base';

import navigateTo from '../../../actions/sideBarNav';
import { openDrawer } from '../../../actions/drawer';
import myTheme from '../../../themes/base-theme';
import styles from './styles';

const launchscreenBg = require('../../../../img/launchscreen-bg.png');
const launchscreenLogo = require('./img/logoWithName.png');
const homeBg = require('../../../../img/home_bg.jpg');
const facebookLoginButton = require('../../../../img/facebook_sign_in_button.png');

class Home extends Component { // eslint-disable-line

  constructor(props) {
    super(props);
    AsyncStorage.getItem('Profile').then((value) => {
      console.log(value);
      if (value == null) {
        AsyncStorage.setItem('Profile', JSON.stringify({
          name: '',
          expertise: 'Software Engineer',
          jobProviderMode: false
        }));
        this.setState({
          name: ""
        });
      } else {
        this.setState({
          name: JSON.parse(value).name
        });
      }
    });
  }

  state = {
    name: ""
  }

  static propTypes = {
    navigateTo: React.PropTypes.func,
    openDrawer: React.PropTypes.func
  }
  
  
  navigateTo(route) {
    this.props.navigateTo(route, 'home');
  }

  facebookLogin() {
      AsyncStorage.setItem('Profile', JSON.stringify({
        name: this.state.name,
        expertise: 'Software Engineer',
        jobProviderMode: false
      }));
     this.navigateTo('gloSlash'); 
  }

  render() {
    return (
      <Container theme={myTheme}>
        <Image source={homeBg} style={styles.imageContainer}>
          <View style={styles.logoContainer}>            
            {/* <Image source={launchscreenLogo} style={[styles.logo, {resizeMode: Image.resizeMode.contain}]} /> */}
            <Text style={styles.titleFont}>Yelperz</Text>
            <Text style={styles.subTitleFont}>Need Help? Just Yell</Text>

          </View>
          <View style={{flex: 1, alignItems: 'center', justifyContent:'center'}}>
            <View>
              <TextInput 
                style={{
                  width: 200, height: 40, 
                  backgroundColor: 'white', textAlign: 'center', 
                  paddingHorizontal: 10,
                  shadowOpacity: 0.8,
                  shadowRadius: 2,
                  shadowOffset: {
                      height: 1,
                      width: 0
                  }}} 
                placeholder="Name for Demo" 
                placeholderTextColor="rgba(150,150,150,1)"
                onChangeText={(text) => this.setState({name: text})}
                value={this.state.name}
                accessibilityLabel={this.props.text || this.props.placeholder}
                underlineColorAndroid="transparent"/>
              </View>
          </View>
          <View style={styles.buttonContainer}>
            <TouchableOpacity style={{alignItems: 'center'}} onPress={() => this.facebookLogin()} >
                <Image source={facebookLoginButton}/>
            </TouchableOpacity> 
          </View>
        </Image>
      </Container>
    );
  }
}

function bindActions(dispatch) {
    return {
        navigateTo: (route, homeRoute) => dispatch(navigateTo(route, homeRoute)),
        openDrawer: () => dispatch(openDrawer()),
    };
}

const mapStateToProps = state => ({
  navigation: state.cardNavigation,
});

export default connect(mapStateToProps, bindActions)(Home);
