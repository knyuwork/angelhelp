/***************************************************************************/
/*** WARNING *** WARNING *** WARNING *** WARNING *** WARNING *** WARNING ***/
/*                                                                         */
/*  Before you want to modify the content here,                            */
/*  Please inform the team first !!!!!!                                    */
/*                                                                         */
/*** WARNING *** WARNING *** WARNING *** WARNING *** WARNING *** WARNING ***/
/***************************************************************************/

var moment = require('moment');   


//Get JobList
export function getJobList(longitude, latitude) {
    
    return fetch('http://api-yelperz.azurewebsites.net/api/getTasks?latitude='+latitude+'&longitude='+longitude)    
        .then((response) => response.json());
        
}

//Post Task
export function postTask(request) {
    var requestBody = request;
    var photos = []; 
    requestBody.photos.forEach(function(entry){
        photos.push(entry.data);
    });

    //var photos = 
    requestBody = {
        ...requestBody,
        photos
    };

    console.log(JSON.stringify(requestBody));
    
    return fetch('http://api-yelperz.azurewebsites.net/api/postTask', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
        },
        body: JSON.stringify(requestBody)
    //}).then((response) => response.json());
    }).then((response) => console.log("Post Task: "+ JSON.stringify(response)));
        
}

//Get Yelling Task
export function getYellingTasks(yellerId) {
    
    return fetch('http://api-yelperz.azurewebsites.net/api/getYellingTasks?id='+yellerId).then((response) => response.json());
        
}

//Get Helping Task
export function getHelpingTasks(helperId) {

    return fetch('http://api-yelperz.azurewebsites.net/api/getHelpingTasks?id='+helperId).then((response) => response.json());
}

//Get Task Detail by yellerId
export function getTaskDetail(taskId) {

    return fetch('http://api-yelperz.azurewebsites.net/api/getTaskDetails?id='+taskId).then((response) => response.json());
}


//Accept Task
export function acceptTask(helperId, taskId) {

    return fetch('http://api-yelperz.azurewebsites.net/api/acceptTask?helperId='+helperId+'&taskId='+taskId).then((response) => response.json());
}

function Test(response) {
    console.log(response.json());
}