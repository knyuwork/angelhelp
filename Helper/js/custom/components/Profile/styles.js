
const React = require('react-native');

const { StyleSheet, Image, Dimensions } = React;
const { width, height } = Dimensions.get('window');

module.exports = StyleSheet.create({
    container: {
        backgroundColor: '#FBFAFA',
    },
    userIcon: {
        width: width/4, 
        height: width/4, 
        borderRadius: width/8, 
        shadowOpacity: 0.8,
        shadowRadius: 2,
    },
    userIconContainer: {
        flex: 1, 
        width: width,
        justifyContent: 'center',
        alignItems: 'center',
        borderTopLeftRadius: 10, 
        borderTopRightRadius: 10
    },
    userInfoText: {
        color: 'white',
        shadowOpacity: 0.8,
        shadowRadius: 2,
        shadowOffset: {
            height: 1,
            width: 0
        }
    },
    /* User Level Related Start */
    userLevelProgressBar: {

        flex: 1, 
        overflow: 'visible',
        justifyContent: 'center',
        alignItems: 'center'
    },
    userLevelContainer: {
        backgroundColor: 'white',  
        width: 30,
        padding: 4, 
        borderColor: 'gray', 
        borderRadius: 30, 
        borderWidth: 2
    },
    userLevelText: {
        backgroundColor: 'rgba(0, 0, 0, 0)'
    },
    /* User Level Related End */
    cardItem: {
        flex: 1, 
        //width: width,
        flexDirection: 'row',
        alignItems: 'center',
        //justifyContent: 'flex-end',
    },
    cardItemValue: {
        flex: 1,
        //flexDirection: 'row',
        alignSelf: 'flex-end',
        justifyContent: 'flex-end',
    },
    cardItemName: {
        flex: 1,
        alignItems: 'flex-start',
        justifyContent: 'flex-start',
    }
});
