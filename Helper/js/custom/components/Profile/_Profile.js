
import React, { Component } from 'react';
import { TouchableOpacity, Image, View, Switch, Text, AsyncStorage, Dimensions, TextInput } from 'react-native';
import { connect } from 'react-redux';
import { Container, Header, Title, Content, Button, Icon, Card, CardItem, Footer} from 'native-base';

import BlurImage from 'react-native-blur-image';
import * as Progress from 'react-native-progress';
const { width, height } = Dimensions.get('window');

import { openDrawer } from '../../../actions/drawer';
import BottomMenu from '../../../components/BottomMenu/BottomMenu';
import styles from './styles';

const camera = require('../../../../img/camera.png');
const userIcon = require('./img/userIcon.jpg');

class Profile extends Component {

    static propTypes = {
        openDrawer: React.PropTypes.func,
    }
    state = {
        name: 'Ernest',
        expertise: 'Software Engineer'
    };

    constructor(props) {
        super(props);
        //this.getAsyncValue('Job_Provider_Mode');
        
    }

    componentDidMount() {
        AsyncStorage.getItem('Profile').then((value) => {
            if (value == null) {
                this.setState({jobProviderMode: false});
            } else {
                this.setState(JSON.parse(value));
                console.log(value);
            }
        }).done();
    }

    async onSave() {
        try {
            await AsyncStorage.mergeItem('Profile', JSON.stringify(this.state));
            //this.setState(object);
        } catch (error) {
            console.log('AsyncStorage error: ' + error.message);
        }
    }

  render() {
    return (
      <Container style={styles.container}>
        <Content>
            <BlurImage
                source={userIcon}
                style={styles.userIconContainer}
                blurRadius={10} >
                <Image style={[styles.userIcon, {marginBottom: -26}]} source={userIcon} >
                    <Progress.Circle size={width/4} progress={0.3} 
                        unfilledColor={'white'} color={'rgba(115, 181, 243, 1)'}
                        style={styles.userLevelProgressBar}>
                        
                    </Progress.Circle>
                </Image>
                <View style={{ width: width, paddingLeft: width*3/8, paddingRight: width*3/8, marginBottom: 10}}>
                    <View style={styles.userLevelContainer}>
                        <Text style={styles.userLevelText}>24</Text>
                    </View>
                </View>
                <View>
                    <TextInput 
                        style={[styles.userInfoText, {textAlign: 'center', width:100, height: 20}]} 
                        value={this.state.name} 
                        onChangeText={(text) => this.setState({name: text})}/>
                </View>
                <Text style={styles.userInfoText}>{this.state.expertise}</Text>
            
            </BlurImage>
            <Content padder>

                <Text style={{marginBottom: 5, color: 'gray'}}>Setting</Text>
                <Card>
                    <CardItem style={styles.cardItem}> 
                        <Text style={styles.cardItemName}>Job Provider Mode</Text>
                        <View style={styles.cardItemValue}>
                            <Switch
                                onValueChange={(value) => this.setState({jobProviderMode: value})}
                                value={this.state.jobProviderMode} />
                        </View>
                    </CardItem>
                </Card>
                <View style={{paddingTop: 10, flex: 1, alignItems:'center', justifyContent: 'center'}}>
                    <TouchableOpacity  
                        onPress={this.onSave}
                        style={{borderRadius: 20, backgroundColor: 'rgba(87,148,165,1)', alignItems:'center', justifyContent: 'center'}}>
                        <Text style={{color: 'white', marginHorizontal: 10, marginVertical: 5}}>Save</Text>
                    </TouchableOpacity>
                </View>
            </Content>
        
        </Content>

        <Footer>
            <BottomMenu activeIndex={3}/>
        </Footer>

      </Container>
    );
  }
}



function bindAction(dispatch) {
  return {
    openDrawer: () => dispatch(openDrawer()),
  };
}

const mapStateToProps = state => ({
  navigation: state.cardNavigation,
});

export default connect(mapStateToProps, bindAction)(Profile);
