
import React, { Component } from 'react';
import { View, Image, TouchableOpacity, Dimensions, AsyncStorage } from 'react-native';
import { connect } from 'react-redux';
import { Text } from 'native-base';


import navigateTo from '../../../../actions/sideBarNav';
import Modal from '../../../modules/react-native-modalbox';
import ImageSlider from 'react-native-image-slider';
import FirebaseClient from '../../../../lib/FirebaseClient';
import styles from './styles';


import ChatBubble from '../../../../components/ChatBubble/ChatBubble';

//Image resources
const { width, height } = Dimensions.get('window');
const jobProviderIcon = require('./img/provider-image.jpeg');
const closeButtonIcon = require('./img/closeButtonIcon.png');
const test = require('./img/test.jpg');
const test2 = require('./img/test2.jpeg');
const test3 = require('./img/test3.jpeg');


const acceptButton = require('./img/acceptButton.png');
const callButton = require('./img/callButton.png');
const rejectButton = require('./img/rejectButton.png');

var modalWidth = width*8/9;
var modalHeight = height*5/6;

class JobDetail extends Component {
    
    static propTypes = {
        isOpen: React.PropTypes.bool.isRequired,
        onClose: React.PropTypes.func
    }
    
    constructor(props) {
        super(props);
        this.navigateTo = this.navigateTo.bind(this);
    }
    
    state = {
        isAccepted: false
    } 

    /* Firebase Related */

    onClose() {
        ref = FirebaseClient.database().ref('/HelpList/'+this.props.jobDetail.helpId+'/status');
        ref.set('Asking');
        this.setState({
            isAccepted: false
        });

    }

    onAccept() {
        console.log(JSON.stringify(this.props.jobDetail));
        
        ref = FirebaseClient.database().ref('/HelpList/'+this.props.jobDetail.helpId+'/status');
        ref.set('Coming');
        this.setState({
            isAccepted: true
        });
    }

    /* Firebase end */

    navigateTo(route) {
        this.props.navigateTo(route, 'home');
    }

    sliderImages() {
        var image = this.props.jobDetail.image;
        console.log('ext: '+image.ext);
        console.log('path: '+image.path);

        var images = [{ uri: 'data:image/'+image.ext+';base64,'+this.props.jobDetail.image.path }];
        /*
        this.props.jobDetail.images.forEach(function(entry) {
            images.push({uri: entry});
        });
        */

        return images;
    }

    renderButton() {
        if (this.state.isAccepted == true) {
            return (
                <View style={[styles.flex_2, {flexDirection: 'row', backgroundColor: 'white', margin: 20, borderRadius: 7}]}>
                    <TouchableOpacity 
                        style={styles.flex_1}>
                        <Image style={styles.Button} source={callButton} />
                    </TouchableOpacity>
                    <TouchableOpacity 
                        onPress={() => {this.props.onClose(); this.onClose() }}
                        style={styles.flex_1}>
                        <Image style={styles.Button} source={rejectButton} />
                    </TouchableOpacity>
                </View>
            );

        } else {
            return (
                <View style={[styles.flex_2, {flexDirection: 'row', backgroundColor: 'white', margin: 20, borderRadius: 7}]}>
                    <TouchableOpacity 
                        onPress={() => {this.onAccept()}}
                        style={[styles.flex_1]}>
                        <Image style={styles.Button} source={acceptButton} />
                    </TouchableOpacity>
                    <TouchableOpacity 
                        style={styles.flex_1}>
                        <Image style={styles.Button} source={callButton} />
                    </TouchableOpacity>
                    <TouchableOpacity 
                        onPress={() => {this.props.onClose(); this.onClose() }}
                        style={styles.flex_1}>
                        <Image style={styles.Button} source={rejectButton} />
                    </TouchableOpacity>
                </View>
            );
            
        }
    }

    render() {
        return (
            
            <Modal style={styles.modal} 
                position={"center"}
                backdrop={true}
                backdropOpacity={0.0}
                backdropColor={'black'}
                backdropPressToClose={false}
                ref={"modal4"}
                swipeToClose={false}
                isOpen={this.props.isOpen}>
                <View style={[styles.detailContainer, styles.flex_2]}>
                    {/* Image Slider */}
                    <View style={[styles.imageSliderContainer, styles.flex_10]}>
                        <ImageSlider height={500} style={{borderRadius: 10}} images={this.sliderImages()}/>
                    </View>
                    
                    {this.renderButton()}
                </View>
            </Modal>
        );
    }
}


function bindAction(dispatch) {
  return {
    openDrawer: () => dispatch(openDrawer()),
    navigateTo: (route, homeRoute) => dispatch(navigateTo(route, homeRoute)),
  };
}

const mapStateToProps = state => ({
  navigation: state.cardNavigation,
});

export default connect(mapStateToProps, bindAction)(JobDetail);
