
const React = require('react-native');
import { StyleSheet, Dimensions, Image } from 'react-native';
const { width, height } = Dimensions.get('window');

var modalWidth = width*8/9;
var modalHeight = height*5/6;

module.exports = StyleSheet.create({
    /* Flex styles */
    flex_1: {
        flex:1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    flex_2: {
        flex:2,
        justifyContent: 'center',
        alignItems: 'center',
    },
    flex_3: {
        flex:3,
        justifyContent: 'center',
        alignItems: 'center',
    },
    flex_5: {
        flex:5,
        justifyContent: 'center',
        alignItems: 'center',
    },
    flex_10: {
        flex:10,
        justifyContent: 'center',
        alignItems: 'center',
    },

    /* Font styles */
    title_font: {
        fontFamily: 'SFNS Display',
        fontWeight: "300",
        color: "white",
        fontSize: 25,
        lineHeight: 40
    },
    info_font: {
        fontFamily: 'SFNS Display',
        fontWeight: "300",
        color: "white",
        fontSize: 16
    },
    name_font: {
        color: "white",
        fontSize: 15
    },
    subtitle_font: {
        color: "white",
        fontSize: 14
    },
    remainTimeUnitFont: {
        fontFamily: 'SFNS Display',
        fontWeight: "300",
        color: "rgba(87,148,165, 1)",
        fontSize: 40,
        lineHeight: 50
    },
    remainTimeValueFont: {
        fontFamily: 'SFNS Display',
        fontWeight: "300",
        color: "white",
        fontSize: 40,
        lineHeight: 50
    },

    /* Component styles */
    modal: {
        backgroundColor: 'rgba(100,100,100,0.8)',
        height: modalHeight, 
        width: modalWidth,
        borderRadius: 10,
        shadowOpacity: 0.8,
        shadowRadius: 2,
        shadowOffset: {
            height: 1,
            width: 0
        }
    },
    imageSliderContainer: {
        width: modalWidth*8/9,
        borderRadius: 10,
        marginTop: 20
    },
    headerContainer: {
        width: modalWidth,
        borderBottomWidth: 1.5,
        borderBottomColor: 'rgba(140,140,140,0.7)',
        flexDirection: 'row'

    },
    jobProviderAndDescContainer: {
        width: modalWidth*8/9,
        flexDirection: 'row'
    },
    remainTimeContainer: {
        flexDirection: 'row',
        width: modalWidth*7/9,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: 'white'
    },
    closeButtonContainer: {
        width: modalWidth*9/10,
        alignItems: 'flex-end'
    },
    detailContainer: {  
        width: modalWidth,
        borderTopLeftRadius: 10, 
        borderTopRightRadius: 10
    },
    jobProviderIcon: {
        width: 70, 
        height: 70, 
        borderRadius: 35, 
        shadowOpacity: 0.8,
        shadowRadius: 2,
    },
    chatButton: {
        width: modalWidth*3/10,
        backgroundColor: 'rgba(87,148,165,1)',
        borderRadius: modalWidth*3/10,
        marginBottom: 20,
        marginTop: 10,
        shadowOpacity: 0.8,
        shadowRadius: 2,
        shadowOffset: {
            height: 1,
            width: 0
        }
    },
    Button: {
        width: 60, 
        height: 60,
        resizeMode: Image.resizeMode.contain, 
        backgroundColor: 'white', 
        borderRadius: 30,
        shadowOpacity: 0.8,
        shadowRadius: 2,
        shadowOffset: {
            height: 1,
            width: 0
        }
    }
});
