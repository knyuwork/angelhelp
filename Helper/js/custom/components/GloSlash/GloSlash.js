
import React, { Component } from 'react';
import { StyleSheet, View, Dimensions, Image, TouchableOpacity, Switch, Keyboard, TouchableWithoutFeedback, AppState } from 'react-native';
import { connect } from 'react-redux';
import { Container, Header, Title, Content, Text, H3, Button, Icon, Footer } from 'native-base';
import MapView from 'react-native-maps';
import PushNotification from 'react-native-push-notification';

import { openDrawer } from '../../../actions/drawer';
import myTheme from '../../../themes/base-theme';
import styles from './styles';
import { PROVIDER_GOOGLE, PROVIDER_DEFAULT } from 'react-native-maps';


import JobMarker from './JobMarker/JobMarker';
import JobDetail from './JobDetail/JobDetail';
import BottomMenu from '../../../components/BottomMenu/BottomMenu';
import PushController from '../../../lib/PushController';
import FirebaseClient from '../../../lib/FirebaseClient';



import myPositionImg from './img/myPositionImg.png';
import myLocationButtonIcon from './img/myLocationButton.png';
import helpIcon from './img/helpIcon.png';
import searchPinIcon from './img/searchPinIcon.png';
import addTaskIcon from './img/addTaskIcon.png';
import filterIcon from './img/filterIcon.png';




const { width, height } = Dimensions.get('window');

const ASPECT_RATIO = width / height;
const LATITUDE = 22.4;
const LONGITUDE = 114.12;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
const SPACE = 0.01;
let id = 0;

class GloSlash extends Component {

    static propTypes = {
        openDrawer: React.PropTypes.func,
    }

    constructor(props) {
        super(props);
        
        
        //this.onMapPress = this.onMapPress.bind(this);
        this.listenForItems = this.listenForItems.bind(this);
        this.closeJobDetail = this.closeJobDetail.bind(this);
        this.openJobDetail = this.openJobDetail.bind(this);
        this.handleAppStateChange = this.handleAppStateChange.bind(this);

        
        this.listenForItems();
       
    }

    listenForItems() {
        ref = FirebaseClient.database().ref('/HelpList/');
        ref.on('value', (snap) => {
            // get children as an array
            var items = [];
            snap.forEach((child) => {
                let taskObject = child.val();
                if (taskObject.status != "Cancelled") {
                    items.push(taskObject);

                }
            });

            items.reverse();

            this.setState({
                helpList: items
            });
        });
    }

    
    state = {
        region: {
            latitude: LATITUDE,
            longitude: LONGITUDE,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA,
        },
        isSearchEnabled: false,
        initialPosition: { 
            coords: {
                latitude: LATITUDE,
                longitude: LONGITUDE,
                accuracy: 0
            }
        },
        lastPosition: {
            coords: {
                latitude: LATITUDE,
                longitude: LONGITUDE,
                accuracy: 0
            }
        },
        helpList: [
            
        ],
        jobDetail: {
            coordinate: {
                longitude: 114.5399996638298,
                latitude: 22.399998784103794
            },
            image: {
                ext: '',
                path: '',
            }
        },
        isJobDetailOpen: false,
        isYelling: false,
        searchPin: []
    };
    
    
    //watchID: ? number = null;
    
    componentDidMount() {
        navigator.geolocation.getCurrentPosition(
            (position) => {
                this.setState({initialPosition: position});
            },
            (error) => alert(JSON.stringify(error)),
            {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000}
        );
        this.watchID = navigator.geolocation.watchPosition((position) => {
            
            this.setState({
                lastPosition: position, 
            });
            var latitude = position.coords.latitude;
            var longitude = position.coords.longitude;
            
            var newRegion ={
                ...this.state.region,
                latitude: position.coords.latitude,
                longitude: position.coords.longitude,
            };

            //this.map.animateToRegion(newRegion);
            this.setState({
                region: newRegion
            });
        });
        AppState.addEventListener('change', this.handleAppStateChange);
        
    }

    handleAppStateChange(appState) {
        if (appState === 'background') {
            let date = new Date(Date.now() + 5500);

            /*
            if (Platform.OS === 'ios') {
                date = date.toISOString();
            }
            */
            PushNotification.localNotificationSchedule({
                message: "Someone needs your help",
                date,
            });
        }
    }


    componentWillUnmount() {
        AppState.removeEventListener('change', this.handleAppStateChange);
        navigator.geolocation.clearWatch(this.watchID);
    }

    /** Job Detail Function Start **/
    openJobDetail(jobDetail = null){

        this.setState({
            jobDetail: jobDetail,
            isJobDetailOpen: true, 
        });
        
        var newRegion ={
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA,
            latitude: jobDetail.location.latitude,
            longitude: jobDetail.location.longitude,
        };

        this.map.animateToRegion(newRegion);
        

    }


    closeJobDetail(){
        console.log('closed job Detail');
        this.setState({isJobDetailOpen: false});
    }



    
    navigateToCurrentPosition() {

        var newRegion ={
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA,
            latitude: this.state.lastPosition.coords.latitude,
            longitude: this.state.lastPosition.coords.longitude,
        };
        
        this.map.animateToRegion(newRegion);
        this.setState({region: newRegion});
        
            
    }

    onRegionChange(region) {
        this.setState({ region });
    }


    /* Render function for components */
    renderUserMarker() {
        return (
            <MapView.Marker
                image={myPositionImg}
                coordinate={this.state.lastPosition.coords} 
                anchor={{ x: 0.5, y: 0.5}}/>
        );
    }

    renderAccuracyCircle() {
        return (
            <MapView.Circle
                center={this.state.lastPosition.coords}
                radius={this.state.lastPosition.coords.accuracy*100}
                fillColor="rgba(156, 200, 231, 0.5)"
                strokeColor="rgba(0,110,200,0.5)"
                zIndex={2}
                strokeWidth={2} />
        );
    }


    renderButtons() {

        return (
            <View style={styles.mapButtonsContainer}>
                <TouchableOpacity 
                    style={styles.button} onPress={() => this.navigateToCurrentPosition()}>
                        <Image style={styles.buttonImg} source={myLocationButtonIcon}/>
                </TouchableOpacity>
            </View> 
        );
    }



    render() {
      
        return (
            <Container theme={myTheme} style={styles.container}>

                <View >
                    <MapView
                      provider={PROVIDER_GOOGLE}
                      ref={ref => { this.map = ref; }}
                      style={styles.map}
                      //onPress={this.onMapPress}
                      initialRegion={this.state.region}
                      onRegionChange={region => this.onRegionChange(region)}
                    >
                        {/* Helping Mode */}
                        {!this.state.isYelling && this.renderUserMarker()}
                        {!this.state.isYelling && this.renderAccuracyCircle()}
                        {!this.state.isYelling && this.state.helpList.map(helpDetail => (
                            <JobMarker 
                                jobDetail={helpDetail} 
                                onPress={this.openJobDetail}/>
                        ))}

                        {/* Universal */}
                        {this.renderButtons()}
                        
                    </MapView> 

                    <JobDetail 
                        isOpen={this.state.isJobDetailOpen} 
                        onClose={this.closeJobDetail}
                        jobDetail={this.state.jobDetail}/>


                </View>
                <Footer >
                    <BottomMenu activeIndex={0}/>
                </Footer>
                <PushController />
            </Container>
        );
    }
}


function bindAction(dispatch) {
  return {
    openDrawer: () => dispatch(openDrawer()),
    closeJobDetail: () => dispatch(closeJobDetail()),
  };
}

const mapStateToProps = state => ({
  navigation: state.cardNavigation,
});

export default connect(mapStateToProps, bindAction)(GloSlash);
