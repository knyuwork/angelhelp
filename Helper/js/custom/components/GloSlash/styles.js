
const React = require('react-native');
import { StyleSheet, Dimensions, Image } from 'react-native';
const { width, height } = Dimensions.get('window');

module.exports = StyleSheet.create({
    container: {
        ...StyleSheet.absoluteFillObject,
        justifyContent: 'flex-end',
        alignItems: 'center',
        flex: 1
    },
    map: {
        //...StyleSheet.absoluteFillObject,
        width: width,
        height: height,
        flex: 1,
    },
    scrollview: {
        alignItems: 'center',
        paddingVertical: 40,
    },
    mapButtonsContainer: { 
        //flex: 1,    //oringinal way which helps to correct the position
        marginTop: height*12/16,
        alignSelf: 'flex-end', 
        justifyContent: 'flex-end'
    },
    yellButtonContainer: { 
        marginTop: 100,
        alignSelf: 'center', 
        justifyContent: 'center',
    },
    yellButton: {
        //marginTop: 100,   //oringinal way which helps to correct the position
        width: width*7/9,
        height: 50,
        alignItems: 'center', 
        justifyContent: 'center',
        backgroundColor: 'white',
        shadowOpacity: 0.6,
        shadowRadius: 2,
        shadowOffset: {
            height: 1,
            width: 0
        }
    },
    yellButtonText: {
        fontSize: 30, 
        lineHeight: 40,
        fontFamily: 'SFNS Display',
        fontWeight: "300",
        color: 'gray'
    },
    button: {
        marginRight: 20,
        backgroundColor: 'rgba(255,255,255, 1)',
        borderRadius: 35,       
        alignItems: 'center',
        justifyContent: 'center',
        width: 45, 
        height: 45, 
        marginBottom: 20,
        shadowOpacity: 0.5,
        shadowRadius: 1,
        shadowOffset: {
            height: 0,
            width: 0
        }
    },
    buttonImg: {
        width: 25, 
        height: 25, 
        resizeMode: Image.resizeMode.contain
    },
});
