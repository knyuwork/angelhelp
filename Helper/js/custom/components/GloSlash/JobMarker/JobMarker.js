
import React, { Component } from 'react';
import { StyleSheet, View, Dimensions, Image } from 'react-native';
import { connect } from 'react-redux';
import { Container, Header, Title, Content, Text, H3, Button, Icon, Footer, FooterTab } from 'native-base';
import MapView from 'react-native-maps';
import { PROVIDER_GOOGLE, PROVIDER_DEFAULT } from 'react-native-maps';
import PriceMarker from './PriceMarker';

const { width, height } = Dimensions.get('window');

const ASPECT_RATIO = width / height;
const LATITUDE = 37.78825;
const LONGITUDE = -122.4324;
const LATITUDE_DELTA = 0.922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
const SPACE = 0.01;

class JobMarker extends Component {
    
    constructor(props) {
        super(props);
        this.state={
            amount: this.props.jobDetail.amount,
        }
        
    }

    render() {
        
        return (
            
            <MapView.Marker 
                coordinate={this.props.jobDetail.location} 
                key={this.props.jobDetail.key}
                onPress={() => {this.props.onPress(this.props.jobDetail)}}>
                <PriceMarker amount={'Help!'} />
            </MapView.Marker>
        );
    }
}


function bindAction(dispatch) {
  return {
    openDrawer: () => dispatch(openDrawer()),
  };
}

const mapStateToProps = state => ({
  navigation: state.cardNavigation,
});

export default connect(mapStateToProps, bindAction)(JobMarker);
