import { StyleSheet, View, Dimensions, Image, TouchableOpacity } from 'react-native';
const { width, height } = Dimensions.get('window');

const ASPECT_RATIO = width / height;
const LATITUDE = 37.78825;
const LONGITUDE = -122.4324;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
const SPACE = 0.01;

export var demoJobList = [    
    {
        coordinate: {
            latitude: LATITUDE,
            longitude: LONGITUDE + 0.02
        },
        offer: {
            currency: 'HKD',
            amount: 98,
        },
        key: 1,
        jobDescription: 'Clean my car',
        jobProviderId: 'Test 01'

    },
    {
        coordinate: {
            latitude: LATITUDE + 0.02,
            longitude: LONGITUDE + 0.02
        },
        offer: {
            currency: 'HKD',
            amount: 99,
        },
        key: 2,
        jobDescription: 'Clean my house',
        jobProviderId: 'Test 02'

    },
    {
        coordinate: {
            latitude: LATITUDE + 0.02,
            longitude: LONGITUDE
        },
        offer: {
            currency: 'HKD',
            amount: 100,
        },
        key: 3,
        jobDescription: 'Pick up starwberry',
        jobProviderId: 'Test 03'

    }    
]