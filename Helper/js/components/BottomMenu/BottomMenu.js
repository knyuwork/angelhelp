
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Icon, Footer, FooterTab } from 'native-base';
import { Image, View } from 'react-native';

import navigateTo from '../../actions/sideBarNav';
import { openDrawer } from '../../actions/drawer';
import myTheme from '../../themes/base-theme';
import styles from './style';

import yellerIcon from './img/yeller-icon.png';

class BottomMenu extends Component {

    static propTypes = {
        openDrawer: React.PropTypes.func,
        navigateTo: React.PropTypes.func,
    }

    constructor(props) {
        super(props);
    }

    componentDidMount(){
        var activeIndex = this.props.activeIndex;
        var tabsState = this.state.tabsState;
        tabsState[activeIndex] = true;
        this.setState({
            tabsState
        });
    }

    state = {
        tabsState: [false, false, false, false, false]
    };

    menuList = [ 
        /* [<tab index>, <tab name>, <navigation string>, <icon name>] */
        {key: 0, tabName: 'Home', navString: 'gloSlash', iconName: 'ios-home-outline'},
        {key: 3, tabName: 'Profile', navString: 'profile', iconName: 'ios-contact-outline'},
    ];

    navigateTo(route) {
        this.props.navigateTo(route, 'home');
    }

  render() {
    return (
        <FooterTab>
          {this.menuList.map(menu => (
            <Button key={menu.key} active={this.state.tabsState[menu.key]} onPress={() => {this.navigateTo(menu.navString)}} >
                {menu.tabName}
                <Icon name={menu.iconName} />
            </Button>
          ))}
        </FooterTab>
    );
  }
}


function bindAction(dispatch) {
  return {
    openDrawer: () => dispatch(openDrawer()),
    navigateTo: (route, homeRoute) => dispatch(navigateTo(route, homeRoute)),
  };
}

const mapStateToProps = state => ({
  navigation: state.cardNavigation,
});

export default connect(mapStateToProps, bindAction)(BottomMenu);







