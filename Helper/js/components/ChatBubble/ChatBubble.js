import React, { PropTypes } from 'react';
import {
  StyleSheet,
  View,
  Text,
} from 'react-native';

const propTypes = {
  fontSize: PropTypes.number,
  color: PropTypes.string,
  height: PropTypes.number,
  style: PropTypes.object,
};

const defaultProps = {
  fontSize: 13,
  color: '#FF5A5F'
};

class ChatBubble extends React.Component {


  constructor(props) {
      super(props);
      //console.log(this.props.color);
  }

  render() {
    const { fontSize, color, style, height } = this.props;
    return (
      <View style={[style, styles.container]}>
        <View style={[styles.arrow, {borderRightColor: color}]} />
        <View style={[styles.bubble, {backgroundColor: color, height: height, padding: 10}]} >
           {this.props.children}
        </View>

      </View>
    );
  }
}

ChatBubble.propTypes = propTypes;
ChatBubble.defaultProps = defaultProps;


const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  bubble: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    //backgroundColor: '#FF5A5F',
    //padding: 2,
    borderRadius: 5
  },
  arrow: {
    backgroundColor: 'transparent',
    borderWidth: 6,
    borderTopWidth: 10,
    borderRightWidth: 14,
    borderColor: 'transparent',
    alignItems: 'center',
    //borderRightColor: '#FF5A5F',
    marginRight: -0.5,
    marginBottom: -25,
  }
});

module.exports = ChatBubble;
