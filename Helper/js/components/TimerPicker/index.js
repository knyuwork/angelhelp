import React, { PropTypes } from 'react';
import {
  StyleSheet,
  View,
  Text,
  Picker
} from 'react-native';
import Modal from 'react-native-modalbox';


const propTypes = {
  fontSize: PropTypes.number,
  color: PropTypes.string,
  height: PropTypes.number,
  style: PropTypes.object,
};

const defaultProps = {
  fontSize: 13,
  color: '#FF5A5F'
};

class TimerPicker extends React.Component {


  constructor(props) {
      super(props);
      //console.log(this.props.color);
  }

  state = {
    language: 'Eng'
  }


  render() {
      const { fontSize, color, style, height } = this.props;
          return (
          <View>
            <Picker
                selectedValue={this.state.language}
                onValueChange={(lang) => this.setState({language: lang})}>
                <Picker.Item label="Java" value="java" />
                <Picker.Item label="JavaScript" value="js" />
            </Picker>
            <Picker
                selectedValue={this.state.language}
                onValueChange={(lang) => this.setState({language: lang})}>
                <Picker.Item label="Java" value="java" />
                <Picker.Item label="JavaScript" value="js" />
            </Picker>
          </View>
      );
  }
}

TimerPicker.propTypes = propTypes;
TimerPicker.defaultProps = defaultProps;


const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  bubble: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    //backgroundColor: '#FF5A5F',
    //padding: 2,
    borderRadius: 5
  },
  arrow: {
    backgroundColor: 'transparent',
    borderWidth: 6,
    borderTopWidth: 10,
    borderRightWidth: 14,
    borderColor: 'transparent',
    alignItems: 'center',
    //borderRightColor: '#FF5A5F',
    marginRight: -0.5,
    marginBottom: -25,
  }
});

module.exports = TimerPicker;
