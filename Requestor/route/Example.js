import React from 'react';
import {
  Image,
  StatusBar,
  StyleSheet,
  TouchableOpacity,
  View,
  Text
} from 'react-native';
import Camera from 'react-native-camera';
import Modal from 'react-native-modalbox';
import FirebaseClient from '../lib/FirebaseClient';
import styles from './styles';


var ReadImageData = require('NativeModules').ReadImageData;


export default class Example extends React.Component {
  constructor(props) {
    super(props);

    this.camera = null;

    this.state = {
      camera: {
        aspect: Camera.constants.Aspect.fill,
        captureTarget: Camera.constants.CaptureTarget.cameraRoll,
        type: Camera.constants.Type.back,
        orientation: Camera.constants.Orientation.auto,
        flashMode: Camera.constants.FlashMode.auto,
      },
      isRecording: false,
      coordinates: {
        latitude: 'unknown',
        longitude: 'unknown',
      },
      helpId: '',
      isModalOpen: false,
      status: ''
    };

    //binding
    this.listenForItems = this.listenForItems.bind(this);
  }


  componentDidMount() {
      navigator.geolocation.getCurrentPosition(
          (position) => {
              this.setState({initialPosition: position});
          },
          (error) => alert(JSON.stringify(error)),
          {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000}
      );
      this.watchID = navigator.geolocation.watchPosition((position) => {
          
          this.setState({
              lastPosition: position, 
          });
          var latitude = position.coords.latitude;
          var longitude = position.coords.longitude;
          
          var coordinates ={
              latitude: position.coords.latitude,
              longitude: position.coords.longitude,
          };

          //this.map.animateToRegion(newRegion);
          this.setState({
              coordinates: coordinates
          });
      });
      
  }

  listenForItems() {
        ref = FirebaseClient.database().ref('/HelpList/'+this.state.helpId);
        ref.on('value', (snap) => {
            let res = JSON.parse(JSON.stringify(snap));
            console.log(JSON.stringify(res[this.state.helpId.toString()].status));
            this.setState({
                status: res[this.state.helpId.toString()].status
            });

        });
    }

  takePicture = () => {
    if (this.camera) {
      this.camera.capture()
        .then((data) => {
          console.log(JSON.stringify(data));
          let pathLength = data.path.length;
          ReadImageData.readImage(data.path, (imageBase64) => {
            var helpId = Math.round(Math.random() * 10000000);
            this.setState({
              helpId,
              isModalOpen: true
            });
            ref = FirebaseClient.database().ref('/HelpList/'+helpId);
            //ref.push({
            ref.set({
                helpId,
                image: {
                  path: imageBase64,
                  ext: data.path.substring(pathLength-3, pathLength)
                },
                location: this.state.coordinates,
                status: 'Asking'
            });
          });
          this.listenForItems();
        })
        .catch(err => console.error(err));
    }
    
  }

  onCancel = () => {
      ref = FirebaseClient.database().ref('/HelpList/'+this.state.helpId+'/status');
      ref.set('Cancelled');
      this.setState({
        isModalOpen: false
      });
  }

  startRecording = () => {
    if (this.camera) {
      this.camera.capture({mode: Camera.constants.CaptureMode.video})
          .then((data) => console.log(data))
          .catch(err => console.error(err));
      this.setState({
        isRecording: true
      });
    }
  }

  stopRecording = () => {
    if (this.camera) {
      this.camera.stopCapture();
      this.setState({
        isRecording: false
      });
    }
  }

  switchType = () => {
    let newType;
    const { back, front } = Camera.constants.Type;

    if (this.state.camera.type === back) {
      newType = front;
    } else if (this.state.camera.type === front) {
      newType = back;
    }

    this.setState({
      camera: {
        ...this.state.camera,
        type: newType,
      },
    });
  }

  get typeIcon() {
    let icon;
    const { back, front } = Camera.constants.Type;

    if (this.state.camera.type === back) {
      icon = require('../assets/ic_camera_rear_white.png');
    } else if (this.state.camera.type === front) {
      icon = require('../assets/ic_camera_front_white.png');
    }

    return icon;
  }

  switchFlash = () => {
    let newFlashMode;
    const { auto, on, off } = Camera.constants.FlashMode;

    if (this.state.camera.flashMode === auto) {
      newFlashMode = on;
    } else if (this.state.camera.flashMode === on) {
      newFlashMode = off;
    } else if (this.state.camera.flashMode === off) {
      newFlashMode = auto;
    }

    this.setState({
      camera: {
        ...this.state.camera,
        flashMode: newFlashMode,
      },
    });
  }

  get flashIcon() {
    let icon;
    const { auto, on, off } = Camera.constants.FlashMode;

    if (this.state.camera.flashMode === auto) {
      icon = require('../assets/ic_flash_auto_white.png');
    } else if (this.state.camera.flashMode === on) {
      icon = require('../assets/ic_flash_on_white.png');
    } else if (this.state.camera.flashMode === off) {
      icon = require('../assets/ic_flash_off_white.png');
    }

    return icon;
  }


  render() {
    return (
      <View style={styles.container}>
        <StatusBar
          animated
          hidden
        />
        <Camera
          ref={(cam) => {
            this.camera = cam;
          }}
          style={styles.preview}
          aspect={this.state.camera.aspect}
          captureTarget={this.state.camera.captureTarget}
          type={this.state.camera.type}
          flashMode={this.state.camera.flashMode}
          onFocusChanged={() => {}}
          onZoomChanged={() => {}}
          defaultTouchToFocus
          mirrorImage={false}
        />
        <View style={[styles.overlay, styles.topOverlay]}>
          <TouchableOpacity
            style={styles.typeButton}
            onPress={this.switchType}
          >
            <Image
              source={this.typeIcon}
            />
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.flashButton}
            onPress={this.switchFlash}
          >
            <Image
              source={this.flashIcon}
            />
          </TouchableOpacity>
        </View>
        <View style={[styles.overlay, styles.bottomOverlay]}>
          {
            !this.state.isRecording
            &&
            <TouchableOpacity
                style={styles.captureButton}
                onPress={this.takePicture}
            >
            {/*
              <Image
                  source={require('../assets/ic_photo_camera_36pt.png')}
              />
            */}
              <Text style={{color: 'red'}}>HELP!!!</Text>
            </TouchableOpacity>
            ||
            null
          }
          {/*
            <View style={styles.buttonsSpace} />
          {
              !this.state.isRecording
              &&
              <TouchableOpacity
                  style={styles.captureButton}
                  onPress={this.startRecording}
              >
                <Image
                    source={require('../assets/ic_videocam_36pt.png')}
                />
              </TouchableOpacity>
              ||
              <TouchableOpacity
                  style={styles.captureButton}
                  onPress={this.stopRecording}
              >
                <Image
                    source={require('../assets/ic_stop_36pt.png')}
                />
              </TouchableOpacity>
          }
          */}
          
        </View>
        <Modal style={styles.modal} backdrop={false} isOpen={this.state.isModalOpen} position={"center"} ref={"modal2"}>
            <View style={{flex: 1, justifyContent: 'center'}}>
              <Text style={{fontSize: 28, lineHeight: 35}}>
                {(this.state.status == "Coming")? 'Your help is coming !!': 'Asking for help ... '}
              </Text>
            </View>
            <View style={{flex: 1, justifyContent: 'center'}}>
              <TouchableOpacity
                  style={[styles.captureButton, {backgroundColor: 'green'}]}
                  onPress={this.onCancel}
              >
                <Text style={{color: 'white'}}>Cancel</Text>
              </TouchableOpacity>
            </View>
        </Modal>
      </View>
    );
  }
}
