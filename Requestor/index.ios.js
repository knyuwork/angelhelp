import React from 'react';
import { AppRegistry } from 'react-native';

import Example from './route/Example';

AppRegistry.registerComponent('Example', () => Example);
